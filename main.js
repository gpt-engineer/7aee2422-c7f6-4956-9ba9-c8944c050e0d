let startTime;
let endTime;
let reactionTimes = [];
const clickArea = document.getElementById('clickArea');
const averageTimeDisplay = document.getElementById('averageTime');
const attemptsDisplay = document.getElementById('attempts');

function startTest() {
    clickArea.style.backgroundColor = 'blue';
    clickArea.textContent = 'Click when the box turns green';
    startTime = new Date();
    setTimeout(() => {
        clickArea.style.backgroundColor = 'green';
        startTime = new Date();
    }, Math.random() * 2000 + 1000);
}

function endTest() {
    endTime = new Date();
    let reactionTime = endTime - startTime;
    reactionTimes.push(reactionTime);
    let sum = reactionTimes.reduce((a, b) => a + b, 0);
    let avg = sum / reactionTimes.length;
    averageTimeDisplay.textContent = Math.round(avg);
    attemptsDisplay.textContent = reactionTimes.length;
    clickArea.style.backgroundColor = 'blue';
    clickArea.textContent = 'Click to start';
}

clickArea.addEventListener('click', () => {
    if (clickArea.style.backgroundColor === 'green') {
        endTest();
    } else {
        startTest();
    }
});

window.onload = startTest;
